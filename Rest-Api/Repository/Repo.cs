﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Api.Repository
{
    public abstract class Repo<T> : IDisposable
    {
        private static readonly object Locker = new object();
        protected readonly HashSet<T> _dataCollection;

        private readonly string _path = $"{typeof(T).Name}.json";

        protected Repo()
        {
            _dataCollection = new HashSet<T>();

            try
            {
                Monitor.Enter(Locker);
                // Read all the json data if the file exists
                if (File.Exists(_path))
                {
                    var jsonFile = File.ReadAllText(_path);
                    _dataCollection = JsonConvert.DeserializeObject<HashSet<T>>(jsonFile) 
                                      ?? new HashSet<T>();

                    return;
                }

                // Create the file
                using (File.Create(_path))
                {
                }
            }
            finally
            {
                Monitor.Exit(Locker);
            }
        }

        /// <summary>
        /// Writes the file changes in the back
        /// </summary>
        protected void WriteChanges()
        {
            // start background task
            Task.Factory.StartNew(() =>
            {
                Monitor.Enter(Locker);

                // Serialize the data and write it to disk
                var data = JsonConvert.SerializeObject(_dataCollection, Formatting.Indented);
                File.WriteAllText(_path, data);

                Monitor.Exit(Locker);
            });
        }

        public void Dispose()
        {
            WriteChanges();
        }
    }
}
