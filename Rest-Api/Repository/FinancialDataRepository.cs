﻿using System;
using System.Linq;
using Composite.Data;

namespace Api.Repository
{
    public class FinancialInformationRepository : Repo<FinancialInformation>
    {

        /// <summary>
        /// Adds a new instance of information to the storage
        /// </summary>
        /// <param name="info"></param>
        public void AddNew(FinancialInformation info)
        {
            _dataCollection.Add(info);

            WriteChanges();
        }

        /// <summary>
        /// Deletes finanaicial information for the customer id
        /// </summary>
        /// <param name="customerId"></param>
        public void Delete(Guid customerId)
        {
            _dataCollection.RemoveWhere(s => s.CustomerId == customerId);

            WriteChanges();
        }

        /// <summary>
        /// Retrieves the financial info with the given customer id
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        public FinancialInformation GetById(Guid customerId)
            => _dataCollection.FirstOrDefault(s => s.CustomerId == customerId);

    }
}
