﻿using System;
using System.Collections.Generic;
using System.Linq;
using Composite.Data;

namespace Api.Repository
{
    public class CustomerRepository : Repo<Customer>
    {
        public ICollection<Customer> GetAllCustomers()
        => _dataCollection;
        

        /// <summary>
        /// Adds the given customer to the repository
        /// </summary>
        /// <param name="customer"></param>
        public void AddCustomer(Customer customer)
        {
            _dataCollection.Add(customer);

            WriteChanges();
        }

        /// <summary>
        /// Retrieves the customer with the given Id
        /// </summary>
        /// <param name="id"> Customer identifier</param>
        /// <returns></returns>
        public Customer GetById(Guid id)
          => _dataCollection.FirstOrDefault(s => s.CustomerId == id);

        public void Update(Customer customer)
        {
           var current = GetById(customer.CustomerId);
           if(current == null)
               throw new NullReferenceException($"Could not find customer with id {customer.CustomerId}");

           _dataCollection.RemoveWhere(c => c.CustomerId == current.CustomerId);
           _dataCollection.Add(customer);

           WriteChanges();
        }

    }
}
