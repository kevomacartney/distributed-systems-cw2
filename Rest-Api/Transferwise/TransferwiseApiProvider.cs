﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Api.Models;
using Api.Repository;
using Api.Transferwise.Config;
using Api.Transferwise.Models;
using Composite.Covalence;
using Composite.Models.Transferwise;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;

namespace Api.Transferwise
{
    public class TransferwiseApiProvider : ITransferwiseApiProvider
    {
        private static class LogEvents
        {
            public static readonly EventId CreatingCustomer = new EventId(1, nameof(CreateCustomerAccount));
            public static readonly EventId HandlingHttpResponse = new EventId(2, nameof(HandleHttpResponseMessage));
        }

        public string ProfileId => _profile.Id.ToString();

        private readonly HttpClient _httpClient;
        private readonly TransferwiseConfig _transferWiseApi;
        private readonly FinancialInformationRepository _financialInfoRepo;

        private readonly ILogger _logger;

        private Profile _profile;

        public TransferwiseApiProvider(HttpClient client, IServiceProvider provider)
        {
            _httpClient = client;
            _transferWiseApi = provider.GetService<TransferwiseConfig>();
            _logger = provider.GetService<ILogger<TransferwiseApiProvider>>();
            _financialInfoRepo = provider.GetService<FinancialInformationRepository>();

            // Retrieve the business profile
            GetProfile();
        }

        public async Task<int> CreateCustomerAccount(Guid customerId, TransferwiseCustomerModel model)
        {
            _logger.LogInformation(LogEvents.CreatingCustomer, "Retrieve Financial information for user {userId}", customerId);

            var financial = _financialInfoRepo.GetById(customerId);

            if (financial == null)
                throw new NullReferenceException($"Could not find customer {customerId} financial information");

            // Validate the requirements
            switch (Enum.Parse<Currency>(model.Currency))
            {
                case Currency.EUR:
                    {
                        if (string.IsNullOrEmpty(financial.IbanNumber))
                            throw new InvalidProgramException($"'{nameof(financial.IbanNumber)}' cannot be null for EUR customers");
                        break;
                    }
                case Currency.GBP:
                    {
                        if (string.IsNullOrEmpty(financial.SortCode) || string.IsNullOrEmpty(financial.AccountNumber))
                            throw new InvalidProgramException($"'{nameof(financial.AccountNumber)}' cannot be null for GBP or EUR customers");

                        break;
                    }
                case Currency.USD:
                    {
                        if (string.IsNullOrEmpty(financial.Abartn))
                            throw new InvalidProgramException($"'{nameof(financial.Abartn)}' cannot be null for USD customers");

                        if (string.IsNullOrEmpty(financial.AccountNumber))
                            throw new InvalidProgramException($"'{nameof(financial.AccountNumber)}' cannot be null for USD customers");

                        if (string.IsNullOrEmpty(financial.AccountType))
                            throw new InvalidProgramException($"'{nameof(financial.AccountType)}' cannot be null for USD customers");

                        if (model.Details.Address == null)
                            throw new InvalidProgramException($"'{nameof(model.Details.Address)}' cannot be null for USD customers");
                        break;
                    }
                case Currency.AUD:
                    {
                        if (string.IsNullOrEmpty(financial.BsbCode))
                            throw new InvalidProgramException($"'{nameof(financial.BsbCode)}' cannon be null for AUD customers");

                        if (string.IsNullOrEmpty(financial.AccountNumber))
                            throw new InvalidProgramException($"'{nameof(financial.AccountNumber)}' cannot be null for AUD customers");
                        break;
                    }
                default:
                    throw new InvalidOperationException("Financial information specifies unknown currency");
            }

            _logger.LogInformation(LogEvents.CreatingCustomer, "Creating Transfer wise account for user {userId}", customerId);

            // Create the request json
            var json = JsonConvert.SerializeObject(model, Settings);

            // Send the post request
            var result = JsonConvert.DeserializeObject<RecipientResponseModel>(await SendPostRequest("accounts", json));

            _logger.LogInformation(LogEvents.CreatingCustomer, "Successfully Transferwise account for user {userId}, transferwise Id {transferId}",
                customerId, result.Id);

            return result.Id;
        }

        public async Task<bool> CustomerExistsAsync(int customerId)
        {
            HttpResponseMessage response;
            using (var request = new HttpRequestMessage(HttpMethod.Get, BuildUrl($"accounts/{customerId}")))
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _transferWiseApi.ApiKey);

                response = await _httpClient.SendAsync(request);
            }

            if (response.IsSuccessStatusCode)
                return true;

            var content = await response.Content.ReadAsStringAsync();

            var error = JsonConvert.DeserializeObject<ErrorModel>(content);

            if (error.Errors != null || error.Errors.Any(e => e.Code == "RECIPIENT_MISSING"))
                return false;

            // Create the object scope
            using (_logger.BeginScope(error))
                _logger.LogInformation(LogEvents.HandlingHttpResponse, "There was an error {error} while checking transferwise customer exist.",
                   error.Error);

            return false;
        }

        public Task DeleteCustomer(int customerId)
            => SendDeleteRequest($"accounts/{customerId}");

        public async Task Transfer(int targetCustomer, decimal amount, Currency currency)
        {
            if (targetCustomer <= 0)
                throw new ArgumentException($"target customer id {targetCustomer} is invalid");

            var qouteId = await CreateQuote(amount, currency);

            var transferId = await CreateTransfer(targetCustomer, qouteId);

            var json = JsonConvert.SerializeObject(new Dictionary<string, string>()
            {
                ["type"] = "BALANCE"
            });

            return;
            var result = JsonConvert.DeserializeObject<dynamic>(await SendPostRequest($"profiles/{ProfileId}/transfers/{transferId}/payments", json, "v3"));

            if (result.status == "COMPLETED")
            {
                _logger.LogInformation("Transaction completed successfully");
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private async Task<string> CreateQuote(decimal amount, Currency currency)
        {
            var json = JsonConvert.SerializeObject(new TransferwiseQuoteModel()
            {
                TargetAmount = amount,
                ProfileId = ProfileId,
                Source = "GBP",
                Target = currency.ToString()
            }, Settings);

            var result = JsonConvert.DeserializeObject<dynamic>(await SendPostRequest("quotes", json));



            return (string)result.id;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="QouteId"></param>
        /// <returns></returns>
        private async Task<string> CreateTransfer(int targetCustomer, string QouteId)
        {
            var json = JsonConvert.SerializeObject(new TransferwiseTransferModel()
            {
                TargetAccount = targetCustomer.ToString(),
                Quote = QouteId,
                Details = new TransferwiseTransferModel.TransferDetails()
                {
                    Reference = "Distributed"
                }
            }, Settings);

            var result = JsonConvert.DeserializeObject<dynamic>(await SendPostRequest("transfers", json));

            return (string)result.id;
        }

        /// <summary>
        /// Requests the transfer wise profile id
        /// </summary>
        /// <returns></returns>
        private void GetProfile()
        {
            var request = new HttpRequestMessage(HttpMethod.Get, BuildUrl("profiles"));
            request.Headers.Add("Authorization", $"Bearer {_transferWiseApi.ApiKey}");

            var result = _httpClient.SendAsync(request).Result;
            result.EnsureSuccessStatusCode();

            // Read the profiles
            var profiles = JsonConvert.DeserializeObject<List<Models.Profile>>(result.Content.ReadAsStringAsync().Result);
            _profile = profiles.FirstOrDefault(p => p.Type == "business");

            if (_profile == null)
                throw new NullReferenceException("Could not retrieve Transferwise business profile");
        }

        #region Helpers

        /// <summary>
        /// Builds the url using the route and returns the fully built url
        /// </summary>
        /// <param name="route"></param>
        /// <returns></returns>
        private string BuildUrl(string route, string version = "v1")
           => $"{_transferWiseApi.Url}{version}/{route}";

        /// <summary>
        /// Sends a patch request to graph
        /// </summary>
        /// <param name="api"></param>
        /// <param name="json"></param>
        /// <returns></returns>
        private async Task<string> SendPostRequest(string api, string json, string version = null)
        {
            string url;
            if (version != null)
                url = BuildUrl(api, version);
            else
                url = BuildUrl(api);

            HttpResponseMessage response;
            using (var request = new HttpRequestMessage(HttpMethod.Post, url))
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _transferWiseApi.ApiKey);

                request.Content = new StringContent(json, Encoding.UTF8, "application/json");

                response = await _httpClient.SendAsync(request);
            }

            return await HandleHttpResponseMessage(response);
        }

        private async Task<string> SendDeleteRequest(string api, string json = "")
        {
            var url = BuildUrl(api);

            HttpResponseMessage response;
            using (var request = new HttpRequestMessage(HttpMethod.Delete, url))
            {
                request.Headers.Authorization = new AuthenticationHeaderValue("Bearer ", _transferWiseApi.ApiKey);

                // Check if json is null
                if (!string.IsNullOrEmpty(json))
                    request.Content = new StringContent(json, Encoding.UTF8, "application/json");

                response = await _httpClient.SendAsync(request);
            }

            return await HandleHttpResponseMessage(response);
        }

        /// <summary>
        /// Marshals the http response and throws an exception if the request was not successful.
        /// </summary>
        /// <param name="response"> The http response message </param>
        /// <returns></returns>
        private async Task<string> HandleHttpResponseMessage(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode)
                return await response.Content.ReadAsStringAsync();

            // Prepare error 
            var error = await response.Content.ReadAsStringAsync();

            var model = JsonConvert.DeserializeObject<ErrorModel>(error);

            using (_logger.BeginScope(model))
                _logger.LogInformation(LogEvents.HandlingHttpResponse, "There was an error {error} while sending request to Transfer wise API",
                   model.Error);

            // Throw web exception with formatted error
            throw new WebException(error);
        }

        private static DefaultContractResolver _contractResolver = new DefaultContractResolver
        {
            NamingStrategy = new CamelCaseNamingStrategy()
        };

        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            NullValueHandling = NullValueHandling.Ignore,
            ContractResolver = _contractResolver,
            Converters =
            {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal },
            },
        };
        #endregion
    }
}
