﻿using Newtonsoft.Json;

namespace Api.Transferwise.Models
{
    /// <summary>
    /// Request to create new Transferwise customer
    /// </summary>
    public class NewCustomerRequest
    {
        /// <summary>
        /// Gets or sets the account holders name (optional)
        /// </summary>
        [JsonProperty(PropertyName = "accountHolderName")]
        public string AccountHolderName { get; set; }

        /// <summary>
        /// Individual branch of an Australian customer (required)
        /// </summary>
        [JsonProperty(PropertyName = "bsbCode")]
        public string BsbCode { get; set; }

        /// <summary>
        /// Gets or sets the customers account number (required)
        /// </summary>
        [JsonProperty(PropertyName = "accountNumber")]
        public string AccountNumber { get; set; }

        /// <summary>
        /// Gets or sets the currency (optional)
        /// </summary>
        [JsonProperty(PropertyName = "currency")]
        public string Currency { get; set; }

        /// <summary>
        /// Gers or sets the International Bank Account Number (required)
        /// </summary>
        public string IBAN { get; set; }

        /// <summary>
        /// Gets or sets the ACH Routing number for US customer (required)
        /// </summary>
        [JsonProperty(PropertyName = "abartn")]
        public string Abartn { get; set; }

        /// <summary>
        /// Gets or sets the account type
        /// </summary>
        [JsonProperty(PropertyName = "accountType")]
        public string AccountType { get; set; }
    }
}
