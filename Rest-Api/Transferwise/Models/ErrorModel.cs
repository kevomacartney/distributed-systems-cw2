﻿using System.Collections.Generic;

namespace Api.Transferwise.Models
{
    /// <summary>
    /// Represents data validation or violation of business rules related errors
    /// </summary>
    public class TransferwiseError
    {
        /// <summary>
        /// Gets or sets the code error
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Gets or sets a message
        /// </summary>
        public string Message { get; set; }

        /// <summary>
        /// Gets or sets the arguments
        /// </summary>
        public List<string> Arguments { get; set; }
    }

    /// <summary>
    /// Represents a transferwise api error result
    /// </summary>
    public class ErrorModel
    {
        /// <summary>
        /// Gets or sets the list of business rules errors
        /// </summary>
        public List<TransferwiseError> Errors { get; set; }

        /// <summary>
        /// Gets or sets the error message
        /// </summary>
        public string Error { get; set; }

        /// <summary>
        /// Gets or sets the error description
        /// </summary>
        public string ErrorDescription { get; set; }

        /// <summary>
        /// Gets or sets the status code
        /// </summary>
        public int Status { get; set; }

        /// <summary>
        /// Gets or sets the exception message
        /// 
        /// </summary>
        public string Exception { get; set; }
    }
}
