﻿using Composite.Covalence;
using Newtonsoft.Json;

namespace Api.Transferwise.Models
{
    /// <summary>
    /// Response model when new recipient is created
    /// </summary>
    public class RecipientResponseModel
    {
        /// <summary>
        /// Gets or sets the user identifier
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        /// <summary>
        /// Get or sets the user identifier
        /// </summary>
        [JsonProperty("profile")]
        public long Profile { get; set; }

        /// <summary>
        /// Gets or sets the account holders name
        /// </summary>
        [JsonProperty("acccountHolderName")]
        public string AcccountHolderName { get; set; }

        /// <summary>
        /// Gets or sets the account's currency
        /// </summary>
        [JsonProperty("currency")]
        public Currency Currency { get; set; }

        /// <summary>
        /// Gets or sets the user's country
        /// </summary>
        [JsonProperty("country")]
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the recipient type
        /// </summary>
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}
