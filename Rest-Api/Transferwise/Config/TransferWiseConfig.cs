﻿namespace Api.Transferwise.Config
{
    public class TransferwiseConfig
    {
        /// <summary>
        /// Gets or sets the api access token
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// Gets or set the api Url
        /// </summary>
        public string Url { get; set; }
    }
}
