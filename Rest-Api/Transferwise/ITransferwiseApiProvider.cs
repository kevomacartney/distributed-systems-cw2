﻿using System;
using System.Threading.Tasks;
using Api.Models;
using Composite.Covalence;
using Composite.Models.Transferwise;

namespace Api.Transferwise
{
    public interface ITransferwiseApiProvider
    {
        string ProfileId { get; }
        /// <summary>
        /// Will create a transfer wise customer account using the information we hold about customer
        /// </summary>
        /// <exception cref="InvalidProgramException"> Thrown when user's financial information is invalid for the requested information</exception>
        /// <returns>Returns the new customer Id</returns>
        Task<int> CreateCustomerAccount(Guid customerId, TransferwiseCustomerModel model);

        /// <summary>
        /// Checks if customer exists and returns true if they do
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task<bool> CustomerExistsAsync(int customerId);

        /// <summary>
        /// Removes the recipent account with the given customerid
        /// </summary>
        /// <param name="customerId"></param>
        /// <returns></returns>
        Task DeleteCustomer(int customerId);

        /// <summary>
        /// Transfers the given amount to the given customer's bank account
        /// </summary>
        /// <param name="targetCustomer"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        Task Transfer(int targetCustomer, decimal amount, Currency currency);
    }
}
