﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Composite.Covalence;

namespace Api.Models
{
    public class AccountDetails
    {
        public string SortCode { get; set; }

        public string AccountNumber { get; set; }

        public string LegalType { get; set; }

        public string BsbCode { get; set; }

        public string Iban { get; set; }

        public CustomerAddress Address { get; set; }

        public string AccountType { get; set; }

        public string Abartn { get; set; }
    }

    public class TransferwiseCustomerModel
    {
        public string Currency { get; set; }

        public string Type { get; set; }

        public string Profile { get; set; }

        public string AccountHolderName { get; set; }

        public AccountDetails Details { get; set; }
    }
}
