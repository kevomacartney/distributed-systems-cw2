﻿using System;
namespace Api.Models
{
    public class TransferwiseQuoteModel
    {
        /// <summary>
        /// Gets or sets the profile id
        /// </summary>
        public string ProfileId { get; set; }

        /// <summary>
        /// Gets or sets the source currency
        /// </summary>
        public string Source { get; set; }

        /// <summary>
        /// Gets or sets the target currency
        /// </summary>
        public string Target { get; set; }

        /// <summary>
        /// Gets the rate type
        /// </summary>
        public string RateType => "FIXED";

        /// <summary>
        /// Gets or sets the target amount for the user to get
        /// </summary>
        public decimal TargetAmount { get; set; }

        /// <summary>
        /// Gets the type funds source type
        /// </summary>
        public string Type => "BALANCE_PAYOUT";
    }
}
