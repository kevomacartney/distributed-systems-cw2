﻿using System;
namespace Api.Models
{
    public class TransferwiseTransferModel
    {
        public class TransferDetails
        {
           public string Reference { get; set; }
        }

        /// <summary>
        /// Gets or sets the Recipient account id
        /// </summary>
        public string TargetAccount { get; set; }

        /// <summary>
        /// Gets or sets the qoute id
        /// </summary>
        public string Quote { get; set; }

        /// <summary>
        /// Gets or sets the transaction id
        /// </summary>
        public string CustomerTransactionId => Guid.NewGuid().ToString();

        /// <summary>
        /// Gets or sets the transfer detail
        /// </summary>
        public TransferDetails Details { get; set; } 
    }
}
