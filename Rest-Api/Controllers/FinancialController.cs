﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading.Tasks;
using Api.Models;
using Api.Quartz.Jobs;
using Api.Repository;
using Api.Transferwise;
using Composite.Covalence;
using Composite.Data;
using Composite.Models.Customers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using Web.Models;

namespace Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [AllowAnonymous]
    public class FinancialController : ControllerBase
    {
        private static class LogEvents
        {
            public static readonly EventId CreatingFinancialInfo = new EventId(1, nameof(Create));
            public static readonly EventId DeletingFinancialInfo = new EventId(2, nameof(Delete));
        }

        private readonly ILogger _logger;
        private readonly ITransferwiseApiProvider _tranferwiseProvider;
        private readonly IScheduler _scheduler;

        private readonly CustomerRepository _customerRepo;
        private readonly FinancialInformationRepository _financialInfoRepo;

        public FinancialController(IServiceProvider serviceProvider)
        {
            _logger = serviceProvider.GetService<ILogger<FinancialController>>();
            _financialInfoRepo = serviceProvider.GetService<FinancialInformationRepository>();
            _customerRepo = serviceProvider.GetService<CustomerRepository>();

            _scheduler = serviceProvider.GetService<IScheduler>();
            _tranferwiseProvider = serviceProvider.GetService<ITransferwiseApiProvider>();
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> Create([FromRoute(Name = "id")]Guid customerId, [FromBody]FinancialInformation model)
        {
            _logger.LogInformation(LogEvents.CreatingFinancialInfo, "Creating financial information for customer {customerId}",
                customerId);

            // Add the information to the repository
            _financialInfoRepo.AddNew(new FinancialInformation()
            {
                Abartn = model.Abartn,
                AccountNumber = model.AccountNumber,
                AccountName = model.AccountName,
                AccountType = model.AccountType,
                BsbCode = model.BsbCode,
                CustomerId = customerId,
                IbanNumber = model.IbanNumber,
                SortCode = model.SortCode
            });

            var customer = _customerRepo.GetById(customerId);

            if (customer == null)
                return NotFound();

            var request = new TransferwiseCustomerModel()
            {
                AccountHolderName = model.AccountName,
                Currency = customer.CustomerCurrency.ToString(),
                Profile = _tranferwiseProvider.ProfileId,
                Details = new AccountDetails()
                {
                    AccountNumber = model.AccountNumber,
                    Abartn = model.Abartn,
                    BsbCode = model.BsbCode,
                    Iban = model.IbanNumber,
                    SortCode = model.SortCode,
                    LegalType = "PRIVATE"
                }
            };


            switch (customer.CustomerCurrency)
            {
                case Currency.USD:
                    request.Type = "aba";

                    request.Details.Address = customer.Address;
                    request.Details.LegalType = "PRIVATE";
                    request.Details.AccountType = "CHECKING";
                    break;
                case Currency.GBP:
                    request.Type = "sort_code";
                    break;
                case Currency.EUR:
                    request.Type = "iban";
                    break;
                case Currency.AUD:
                    request.Type = "australian";
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }


            // Add the details to transfer wise
            var transferId = await _tranferwiseProvider.CreateCustomerAccount(customerId, request);

            customer.TransferWiseId = transferId;
            _customerRepo.Update(customer);

            return Ok();
        }

        [HttpGet("{id}")]
        public IActionResult Get([FromRoute(Name = "id")]Guid customerId)
        {
            return Ok((_financialInfoRepo.GetById(customerId)));
        }
        public IActionResult Delete([FromRoute(Name = "id")]Guid customerId)
        {
            _financialInfoRepo.Delete(customerId);

            _logger.LogInformation(LogEvents.DeletingFinancialInfo, "Successfully delete financial information for customer {customerId}");
            return Ok();
        }

        [HttpPost("{id}")]
        public IActionResult SendToUser([FromRoute(Name = "id")] Guid customerId, [FromBody]SendMoneyModel model)
        {
            var customer = _customerRepo.GetById(customerId);
            var targetCustomer = _customerRepo.GetById(model.Customer);

            targetCustomer.Balance += model.Amount;
            customer.Balance -= model.Amount;

            // Update the customer
            _customerRepo.Update(customer);
            _customerRepo.Update(targetCustomer);

            return Ok();
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> TransferFunds([FromRoute(Name = "id")] Guid customerId,
            [FromBody]TransferModel transferModel)
        {


            var targetCustomer = _customerRepo.GetById(transferModel.TargetCustomer);

            var targetFinInf = _financialInfoRepo.GetById(transferModel.TargetCustomer);

            if (targetFinInf == null)
                throw new InvalidOperationException("Cannot transfer to an unregistered customer");

            // Create the job description
            var job = JobBuilder.Create<TransferJob>()
                .RequestRecovery(true)
                .UsingJobData("CustomerId", targetCustomer.CustomerId.ToString())
                .UsingJobData("Amount", transferModel.TransferAmount.ToString(CultureInfo.InvariantCulture))
                .Build();

            // trigger builder creates simple trigger
            var trigger = TriggerBuilder.Create()
                .ForJob(job);

            if (transferModel.TransferNow)
                trigger.StartNow();
            else
                trigger.StartAt(transferModel.TransferDate.Value);

            // Schedule the job
            await _scheduler.ScheduleJob(job, trigger.Build());

            return Ok();
        }
    }
}
