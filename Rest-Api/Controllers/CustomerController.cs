﻿using System;
using Api.Repository;
using Composite;
using Composite.Covalence;
using Composite.Data;
using Composite.Models.Customers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Api.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [AllowAnonymous]
    public class CustomerController : Controller
    {
        private readonly CustomerRepository _customerRepo;
        private readonly IConfiguration _configuration;
        private readonly EncryptService _encryptService;

        public CustomerController(IServiceProvider provider)
        {
            _customerRepo = provider.GetService<CustomerRepository>();
            _configuration = provider.GetService<IConfiguration>();
            _encryptService = provider.GetService<EncryptService>();
        }

        [HttpGet("{id}")]
        public IActionResult ById([FromRoute(Name = "id")]Guid customerId)
        {
            var customer = _customerRepo.GetById(customerId);

            if (customer == null)
                return NotFound();

            return Json(customer);
        }

        [HttpGet]
        public IActionResult All()
            => Json(_customerRepo.GetAllCustomers());

        [HttpPost]
        public IActionResult CreateNew([FromBody]Customer customer)
        {
            if (!ModelState.IsValid)
                return BadRequest("Please check the model");

            // Create user
            customer.CustomerId = Guid.NewGuid();
            customer.TransferWiseId = 0;

            SetCurrency(customer);

            // Add the customer
            _customerRepo.AddCustomer(customer);
            // Return the customer Id
            return Ok(customer.CustomerId);
        }

        [HttpPost("{id}")]
        public IActionResult AddFunds([FromRoute(Name = "id")]Guid customerId,
            [FromQuery(Name = "sig")]string sig, [FromBody]AddFundsModel model)
        {
            var json = JsonConvert.SerializeObject(model);

            // Verify the message
            if (!_encryptService.VerifySignature(sig, json))
                return BadRequest("Could not verify the signature of the payload.");
            if (model.CreatedOn > DateTime.UtcNow.AddMinutes(1))
                return BadRequest("The request has expired.");

            var customer = _customerRepo.GetById(customerId);

            if (customer == null)
                return BadRequest("Could not find a customer with the given ID");

            customer.Balance += model.Amount;

            _customerRepo.Update(customer);
            return Ok();
        }

        public IActionResult Update([FromRoute(Name = "id")]Guid customerId, Customer customer)
        {
            var current = _customerRepo.GetById(customerId);

            current.FirstName = customer.FirstName;
            current.LastName = customer.LastName;
            current.Address = customer.Address;

            // Set the customers currency
            SetCurrency(customer);

            _customerRepo.Update(current);
            return Ok();
        }

        /// <summary>
        /// Sets the customers currency
        /// </summary>
        /// <param name="customer"></param>
        private static void SetCurrency(Customer customer)
        {
            switch (customer.Address.Country)
            {
                case "United Kingdom":
                {
                    customer.CustomerCurrency = Currency.GBP;
                    break;
                }
                case "Australia":
                {
                    customer.CustomerCurrency = Currency.AUD;
                    break;
                }
                case "United States":
                {
                    customer.CustomerCurrency = Currency.USD;
                    break;
                }
                default:
                    throw new InvalidOperationException($"Country {customer.Address.Country} cannot be accepted");
            }
        }
    }
}
