﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Api.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ApiKeyValidator
    {
        private readonly RequestDelegate _next;

        public ApiKeyValidator(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var config = httpContext.RequestServices.GetService<IConfiguration>();
            var logger = httpContext.RequestServices.GetService<ILogger<ApiKeyValidator>>();

            var permittedKeys = config.GetSection("ApiKeys:Permitted").Get<List<string>>();

            if (httpContext.Request.Headers.Any(s => s.Key == "Authorization"))
            {
                logger.LogInformation("Authorization header found");

                string key = httpContext.Request.Headers["Authorization"];
              //  var str = key.Remove(0, "Bearer ".Count());

                // Check if the provided key is part of the api
                if (permittedKeys.Contains(key.Remove(0, "Bearer ".Count())))
                {
                    logger.LogInformation("Api key successfully validated");

                    await _next(httpContext);
                    return;
                }

                httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                await httpContext.Response.WriteAsync("Invalid api-key provided");

                logger.LogInformation("Provided Api was not valid");

                return;
            }

            logger.LogWarning("Client did not provide a valid api key");

           // httpContext.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
            await httpContext.Response.WriteAsync("No api-key provided");

            return;
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ApiKeyValidatorExtensions
    {
        public static IApplicationBuilder UseApiKeyMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ApiKeyValidator>();
        }
    }
}
