﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Api.Repository;
using Api.Transferwise;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;

namespace Api.Quartz.Jobs
{
    public class TransferJob : IJob
    {
        private readonly ITransferwiseApiProvider _transferwiseApi;
        private readonly ILogger _logger;
        private readonly CustomerRepository _customerRepo;

        public TransferJob(IServiceProvider serviceProvider)
        {
            _transferwiseApi = serviceProvider.GetService<ITransferwiseApiProvider>();

            _customerRepo = serviceProvider.GetService<CustomerRepository>();
            _logger = serviceProvider.GetService<ILogger<TransferJob>>();
        }

        public async Task Execute(IJobExecutionContext context)
        {
            var st = new Stopwatch();
            st.Start();

            var targetCustomer = (string)context.JobDetail.JobDataMap["CustomerId"];
            var transferAmount = decimal.Parse((string) context.JobDetail.JobDataMap["Amount"]);

            var customer = _customerRepo.GetById(Guid.Parse(targetCustomer));

            // Do the transfer
            await _transferwiseApi.Transfer(customer.TransferWiseId, transferAmount, customer.CustomerCurrency);

            // Update the customer
            customer.Balance -= transferAmount;
            _customerRepo.Update(customer);

            _logger.LogInformation("Successfully transfer {amount} to {customerId}", 
                transferAmount, targetCustomer);

            st.Stop();
        }
    }
}
