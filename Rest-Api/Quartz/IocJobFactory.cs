﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Quartz;
using Quartz.Spi;

namespace Api.Quartz
{
    public class IocJobFactory : IJobFactory
    {
        private static class LogEvents
        {
            public static readonly EventId ActivatingJob = new EventId(1, nameof(NewJob));
            public static readonly EventId FinalizingJob = new EventId(2, nameof(ReturnJob));
        }

        private readonly IServiceProvider _serviceProvider;
        private readonly Dictionary<IJob, IServiceScope> _jobScopes;

        private readonly ILogger _logger;

        public IocJobFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;

            _logger = serviceProvider.GetService<ILogger<IocJobFactory>>();
            _jobScopes = new Dictionary<IJob, IServiceScope>();
        }
        public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var details = bundle.JobDetail;
            var scope = _serviceProvider.CreateScope();
            using (_logger.BeginScope(new Dictionary<string, object>()
            {
                ["jobDescription"] = details.Description,
                ["jobGroup"] = details.Key.Group,
                ["requestRecovery"] = details.RequestsRecovery

            }))
            {
                _logger.LogInformation(LogEvents.ActivatingJob,
                    "Activating job of type {jobType} and identity name {jobName}",
                    details.JobType, details.Key.Name);

                // Resolve the job and register it with its scope
                var job = (IJob)scope.ServiceProvider.GetService(details.JobType);
                _jobScopes.Add(job, scope);

                _logger.LogInformation(LogEvents.ActivatingJob, "Successfully activated Job");
                return job;
            }
        }

        public void ReturnJob(IJob job)
        {

            _logger.LogInformation(LogEvents.FinalizingJob, "Job {jobType} completed execution",
                job.GetType());

            // Dispose the scope, it will also handle the disposal of the job
            var scope = _jobScopes[job];
            scope.Dispose();

            _logger.LogInformation(LogEvents.FinalizingJob, "Successfully disposed job {jobType} service scope", job.GetType());
        }
    }
}
