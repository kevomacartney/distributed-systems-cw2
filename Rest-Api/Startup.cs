﻿using Api.Middleware;
using Api.Quartz;
using Api.Quartz.Jobs;
using Api.Repository;
using Api.Transferwise;
using Api.Transferwise.Config;
using Composite;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;

namespace Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Registers the transferwise configuration as an instance per request
            services.AddTransient(s =>
            {
                var config = s.GetService<IConfiguration>();

                // Create the configuration
                var twConfig = new TransferwiseConfig();
                config.Bind("Transferwise", twConfig);

                return twConfig;
            });

            // Add the api provider as a httpclient
            services.AddHttpClient<ITransferwiseApiProvider, TransferwiseApiProvider>();
            services.AddScoped<FinancialInformationRepository>();
            services.AddScoped<CustomerRepository>();


            // Register the job factory
            services.AddTransient<IJobFactory, IocJobFactory>();

            // Register the scheduler factory
            services.AddSingleton<IScheduler>(s =>
            {
                var stdSchedulerFactory = new StdSchedulerFactory();


                var scheduler = stdSchedulerFactory.GetScheduler().Result;

                // Job factory
                scheduler.JobFactory = s.GetService<IJobFactory>();

                // Start the schedulers
                scheduler.Start().Wait();
                return scheduler;
            });

            services.AddScoped(s =>
            {
                var certSerialNum = Configuration.GetValue<string>("Signing:CertSerialNum");
                var encryptService = new EncryptService(certSerialNum);

                return encryptService;
            });

            // Register the transfer job
            services.AddScoped<TransferJob>();

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseApiKeyMiddleware();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
