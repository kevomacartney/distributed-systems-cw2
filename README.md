#Running
Install Visual studio (Mac or Windows) and ensure you include the ASP.NET CORE package in the installer. 
https://dotnet.microsoft.com/download/dotnet-core/2.2

Go to the URL above, download and install the SDK

Open "Distributed-Cw2.sln" by double clicking it. 

After the project has started up, click the "Start Debugging" button at top which will restore packages and launch the application.

