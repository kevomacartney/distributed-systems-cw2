﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Web.Models;

namespace Web.Api
{
    public class Provider
    {
        private readonly HttpClient _httpClient;
        private readonly ApiModel _apiModel;

        public Provider(IServiceProvider serviceProvide, IHttpClientFactory clientFactory)
        {
            _httpClient = clientFactory.CreateClient();
            _apiModel = serviceProvide.GetService<ApiModel>();
        }

        public async Task<string> SendGetRequest(string controller, string json = "")
        {
            var request = new HttpRequestMessage(HttpMethod.Get, $"{_apiModel.BaseUrl}api/{controller}");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _apiModel.Key);

            if (!string.IsNullOrEmpty(json))
                request.Content = new StringContent(json, Encoding.UTF8, "application/json");

            using (var response = await _httpClient.SendAsync(request))
            {
                // Throw if not successful
                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
        }

        public async Task<string> SendPostRequest(string controller, string json)
        {
            var request = new HttpRequestMessage(HttpMethod.Post, $"{_apiModel.BaseUrl}api/{controller}");
            request.Headers.Authorization = new AuthenticationHeaderValue("Bearer", _apiModel.Key);

            request.Content = new StringContent(json, Encoding.UTF8, "application/json");

            using (var response = await _httpClient.SendAsync(request))
            {
                // Throw if not successful
                response.EnsureSuccessStatusCode();

                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}
