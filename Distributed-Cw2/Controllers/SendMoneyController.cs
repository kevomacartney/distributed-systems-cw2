﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Composite.Data;
using Composite.Models.Customers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Web.Models;

namespace Web.Controllers
{
    public class SendMoneyController : Controller
    {
        private readonly Api.Provider _provider;

        public SendMoneyController(IServiceProvider provider)
        {
            _provider = provider.GetService<Api.Provider>();
        }

        [HttpGet]
        public async Task<IActionResult> SendToAccount([FromRoute(Name = "id")] Guid customerId)
        {
            var customers = JsonConvert.DeserializeObject<List<Customer>>(await _provider.SendGetRequest($"customer/All"));

            var thisCustomer = customers.FirstOrDefault(c => c.CustomerId == customerId);

            if (thisCustomer == null)
                throw new NullReferenceException($"Could not find customer with id {customerId}");

            ViewData["SourceCustomer"] = thisCustomer.FirstName;

            // don't let customer send to themselves
            customers = customers.Where(c => c.CustomerId != customerId).ToList();


            // Create the customer select
            ViewData["Customer"] =
                new SelectList(customers, "CustomerId", "FirstName");

            return View();
        }

        [HttpPost]
        public async Task<IActionResult> SendToAccount([FromRoute(Name = "id")]Guid customerId, SendMoneyModel model)
        {
            if (!ModelState.IsValid)
                return View(model);

            var customer = JsonConvert.DeserializeObject<Customer>(await _provider.SendGetRequest($"customer/ById/{customerId}"));

            if (customer.Balance - model.Amount < 0)
            {
                ViewData["SourceCustomer"] = customer.FirstName;

                var customers = JsonConvert.DeserializeObject<List<Customer>>
                    (await _provider.SendGetRequest($"customer/All")).Where(c => c.CustomerId != customerId);

                ViewData["Customer"] =
                    new SelectList(customers, "CustomerId", "FirstName");

                ModelState.AddModelError("Amount", "Insufficient funds");
                return View();
            }

            await _provider.SendPostRequest($"Financial/SendToUser/{customerId}", JsonConvert.SerializeObject(model));

            return RedirectToAction("Index", "Customers");
        }

        [HttpGet]
        public async Task<IActionResult> Withdraw([FromQuery(Name = "Customer")]Guid customerId)
        {
            var customers = JsonConvert.DeserializeObject<List<Customer>>
                (await _provider.SendGetRequest($"customer/All"));

            // Create the customer select
            ViewData["Customer"] =
                new SelectList(customers, "CustomerId", "FirstName", customerId);

            ViewData["CustomerId"] = customerId;

            if (customerId == Guid.Empty)
                return View();

            var financial =
                JsonConvert.DeserializeObject<FinancialInformation>(
                    await _provider.SendGetRequest($"Financial/Get/{customerId}"));

            if (financial == null)
                return View();

            var model = new WithdrawModel()
            {
                Abartn = financial.Abartn,
                AccountName = financial.AccountName,
                AccountNumber = financial.AccountNumber,
                AccountType = financial.AccountType,
                BsbCode = financial.BsbCode,
                IbanNumber = financial.IbanNumber,
                SortCode = financial.SortCode
            };

            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> WithDraw([FromForm(Name = "Customer")]Guid customerId, WithdrawModel model)
        {
            var targetCustomer
                = JsonConvert.DeserializeObject<Customer>(await _provider.SendGetRequest($"customer/ById/{customerId}"));

            // Create transferwise customer
            if (targetCustomer.TransferWiseId == 0)
            {
                var finModel = new FinancialInformation()
                {
                    Abartn = model.Abartn,
                    AccountName = model.AccountName,
                    AccountNumber = model.AccountNumber,
                    AccountType = model.AccountType,
                    BsbCode = model.BsbCode,
                    CustomerId = customerId,
                    IbanNumber = model.IbanNumber,
                    SortCode = model.SortCode
                };

                // Create transferwise account
                await _provider.SendPostRequest($"Financial/Create/{customerId}", JsonConvert.SerializeObject(finModel));
            }

            await _provider.SendPostRequest($"Financial/TransferFunds/{customerId}",
                JsonConvert.SerializeObject(new TransferModel()
                {
                    TransferAmount = targetCustomer.Balance,
                    TargetCustomer = targetCustomer.CustomerId,
                    TransferNow = true
                }));

            return RedirectToAction(nameof(WithDraw), new {Customer = customerId});
        }
    }
}
