﻿using Composite;
using Composite.Models.Customers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using System.Web;
using Braintree;
using Web.Models;
using Customer = Composite.Data.Customer;
using System.Diagnostics;

namespace Web.Controllers
{
    public class CustomersController : Controller
    {
        private readonly Api.Provider _provider;
        private readonly ILogger _logger;

        private readonly IBraintreeGateway _gateway;
        private readonly EncryptService _encryptService;

        public CustomersController(IServiceProvider services, IHttpClientFactory client)
        {
            _provider = services.GetService<Api.Provider>();
            _encryptService = services.GetService<EncryptService>();

            _gateway = services.GetService<IBraintreeGateway>();
            _logger = services.GetService<ILogger<CustomersController>>();
        }

        // GET: /<controller>/
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            _logger.LogInformation("Retrieving customer information");



            var customers = JsonConvert.DeserializeObject<List<Customer>>(await _provider.SendGetRequest("customer/All"));

            return View(customers);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Update(Customer model)
        {
            if (!ModelState.IsValid)
                return RedirectToAction(nameof(Index));

            var json = JsonConvert.SerializeObject(model);
            await _provider.SendPostRequest("customer/update", json);

            return RedirectToAction(nameof(Index));
        }

        [HttpPost]
        public async Task<IActionResult> AddBalance([FromRoute(Name = "id")]Guid customerId, AddBalanceModel balanceModel)
        {

            if (!ModelState.IsValid)
                return View(balanceModel);


            var customer = JsonConvert.DeserializeObject<Customer>(await _provider.SendGetRequest($"customer/ById/{customerId}"));

            var transaction = new TransactionRequest()
            {
                Amount = balanceModel.Amount,
                BillingAddress = new AddressRequest()
                {
                    PostalCode = customer.Address.PostalCode,
                    StreetAddress = customer.Address.FirstLine,
                    FirstName = customer.FirstName,
                    LastName = customer.LastName,
                    CountryName = customer.Address.Country,
                    Region = customer.Address.City
                },
                CreditCard = new TransactionCreditCardRequest()
                {
                    CardholderName = $"{customer.FirstName} {customer.LastName}",
                    CVV = balanceModel.Cvv,
                    ExpirationYear = balanceModel.ExpiryYear,
                    ExpirationMonth = balanceModel.ExpiryMonth,
                    Number = balanceModel.CreditCard
                }
            };

            try
            {
                var result = await _gateway.Transaction.SaleAsync(transaction);
                // Validate errors
                if (!result.IsSuccess())
                {
                    ModelState.AddModelError("CreditCard", result.Message);
                    return View(balanceModel);
                }
            }
            catch (Exception e)
            {
                ModelState.AddModelError("CreditCard", "There was an with the payment processor");
                return View(balanceModel);
            }


            // Model to add customer funds
            var model = new AddFundsModel()
            {
                Amount = balanceModel.Amount,
                CreatedOn = DateTime.UtcNow
            };

            // Get the model json and create signature
            var json = JsonConvert.SerializeObject(model);
            var sig = _encryptService.Sign(json);

            // Send the request
            await _provider.SendPostRequest($"customer/AddFunds/{customerId}?sig={HttpUtility.UrlEncode(sig)}", json);

            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        public async Task<IActionResult> AddBalance([FromRoute(Name = "id")]Guid customerId)
        {
            var customer = JsonConvert.DeserializeObject<Customer>(await _provider.SendGetRequest($"customer/ById/{customerId}"));
            ViewData["CustomerName"] = customer.FirstName;
            ViewData["Balance"] = customer.Balance;

            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        public async Task<IActionResult> Create(Customer customer)
        {
            customer.CustomerId = Guid.NewGuid();

            var json = JsonConvert.SerializeObject(customer);
            await _provider.SendPostRequest("customer/createnew", json);

            return RedirectToAction(nameof(Index));
        }
    }
}
