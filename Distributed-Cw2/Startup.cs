﻿using Braintree;
using Composite;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Web.Api;
using Web.Models;

namespace Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddHttpClient();

            services.AddScoped<Provider>();
            services.AddScoped<ApiModel>(s =>
            {
                var config = s.GetService<IConfiguration>();

                var apiConfig = new ApiModel();
                config.Bind("Api", apiConfig);

                return apiConfig;
            });

            services.AddScoped(s =>
            {
                var certSerialNum = Configuration.GetValue<string>("Signing:CertSerialNum");
                var encryptService = new EncryptService(certSerialNum);

                return encryptService;
            });

            services.AddScoped(s =>
            {
                var btConfig = new BraintreeConfig();
                var config = s.GetService<IConfiguration>();

                config.Bind("Braintree", btConfig);

                return btConfig;
            });

            services.AddScoped<IBraintreeGateway>(s =>
            {
                var config = s.GetService<BraintreeConfig>();
                
                // create the gateway
                var bt = new BraintreeGateway(Environment.SANDBOX,
                    config.MarchantId,
                    config.PublicKey,
                    config.PrivateKey);

                return bt;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
