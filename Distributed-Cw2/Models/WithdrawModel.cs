﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Composite.Covalence;

namespace Web.Models
{
    public class WithdrawModel
    {
        /// <summary>
        /// Gets or sets the target user's country
        /// </summary>
        public string Country { get; set; }

        /// <summary>
        /// Gets or sets the IBAN number
        /// </summary>
        /// <value>The IBAN number.</value>
        public string IbanNumber { get; set; }

        /// <summary>
        /// Individual branch of an Australian customer
        /// </summary>
        public string BsbCode { get; set; }

        /// <summary>
        /// Gets or sets the USD account type, CHECKING OR SAVING
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// Gets or sets the customers account number
        /// </summary>
        public string AccountNumber { get; set; }

        public string SortCode { get; set; }

        /// <summary>
        /// Gets or sets the account name
        /// </summary>
        /// <value>The name of the account.</value>
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the ACH Routing number for US customer 
        /// </summary>
        public string Abartn { get; set; }

        /// <summary>
        /// Gets or sets the date to transfer
        /// </summary>
        public DateTime TransferAt { get; set; }
    }
}
