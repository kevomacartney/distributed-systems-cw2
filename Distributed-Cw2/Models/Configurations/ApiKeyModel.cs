﻿namespace Web.Models
{
    public class ApiModel
    {
        /// <summary>
        /// Gets or sets the api key
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Gets or sets the base url
        /// </summary>
        public string BaseUrl { get; set; }
    }
}
