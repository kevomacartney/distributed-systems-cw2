﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class BraintreeConfig
    {
        /// <summary>
        /// Gets or sets the merchant id
        /// </summary>
        public string MarchantId { get; set; }

        /// <summary>
        /// Gets or sets the public key
        /// </summary>
        public string PublicKey { get; set; }

        /// <summary>
        /// Gets or sets the private key
        /// </summary>
        public string PrivateKey { get; set; }
    }
}
