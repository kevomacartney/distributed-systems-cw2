﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class AddBalanceModel
    {
        /// <summary>
        /// Gets or sets the amount to add to balance and charge customer
        /// </summary>
        [Required]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the credit card number
        /// </summary>
        [Required]
        public string CreditCard { get; set; }

        /// <summary>
        /// Gets or sets the credit cards cvv
        /// </summary>
        public string Cvv { get; set; }

        /// <summary>
        /// Gets or sets the expiry year
        /// </summary>
        [Required]
        public string ExpiryYear { get; set; }

        /// <summary>
        /// Gets or sets the expiry month
        /// </summary>
        [Required]
        public string ExpiryMonth { get; set; }
    }
}
