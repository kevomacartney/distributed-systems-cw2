﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Web.Models
{
    public class SendMoneyModel
    {
        /// <summary>
        /// Gets or sets the amount to send
        /// </summary>
        [Required]
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the id of the customer to receive the money
        /// </summary>
        [Required]
        public Guid Customer { get; set; }
    }
}
