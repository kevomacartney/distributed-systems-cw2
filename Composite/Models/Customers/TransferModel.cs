﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Composite.Models.Customers
{
    public class TransferModel
    {
        /// <summary>
        /// Gets or sets the target customer's id
        /// </summary>
        public Guid TargetCustomer { get; set; }

        /// <summary>
        /// Gets or sets the transfer amount
        /// </summary>
        public decimal TransferAmount { get; set; }

        /// <summary>
        /// Gets or sets a boolean indicating if the transfer should be done now
        /// </summary>
        public bool TransferNow { get; set; }

        /// <summary>
        /// If <see cref="TransferNow"/> is false, gets or sets the date to transfer
        /// </summary>
        public DateTime? TransferDate { get; set; }
    }
}
