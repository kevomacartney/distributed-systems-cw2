﻿using System;
namespace Composite.Models.Customers
{
    public class AddFundsModel
    {
        /// <summary>
        /// Gets or sets the amount to add to the balance
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Gets or sets the date the request was created
        /// </summary>
        public DateTime CreatedOn { get; set; }
    }
}
