﻿using System;
using Composite.Covalence;

namespace Composite.Models.Transferwise
{
    public class CreateCustomerAccount
    {
        /// <summary>
        /// Gets or sets the user identifier
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// Gets or sets the customers currency
        /// </summary>
        public Currency CustomerCurrency { get; set; }

        /// <summary>
        /// Gets or sets the user's address
        /// </summary>
        public CustomerAddress Address { get; set; }
    }
}
