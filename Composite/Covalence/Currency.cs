﻿namespace Composite.Covalence
{
    public enum Currency
    {
        GBP,
        EUR,
        USD,
        AUD
    }
}
