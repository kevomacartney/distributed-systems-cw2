﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Composite.Covalence
{
    /// <summary>
    /// Represents the address of a given customer
    /// </summary>
    public class CustomerAddress
    {
        /// <summary>
        /// Gets or sets the postal code
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "postCode")]
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the street address
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "firstLine")]
        public string FirstLine { get; set; }

        /// <summary>
        /// Gets or sets the city
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "city")]
        public string City { get; set; }


        /// <summary>
        /// Gets ro sets the state
        /// </summary>
        [JsonProperty(PropertyName = "state")]
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the country
        /// </summary>
        [Required]
        [JsonProperty(PropertyName = "country")]
        public string Country { get; set; }

    }
}
