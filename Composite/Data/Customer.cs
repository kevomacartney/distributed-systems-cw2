﻿using System;
using System.ComponentModel.DataAnnotations;
using Composite.Covalence;

namespace Composite.Data
{
    public class Customer
    {
        /// <summary>
        /// Gets or sets our customer identifier
        /// </summary>
        [Required]
        public Guid CustomerId { get; set; }
        
        /// <summary>
        /// Gets or sets the transfer wise Id
        /// </summary>
        public int TransferWiseId { get; set; }

        /// <summary>
        /// Gets or sets the first name
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// Gets or sets the name
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// Gets or sets the customer currency
        /// </summary>
        [Required]
        public Currency CustomerCurrency { get; set; }

        /// <summary>
        /// Gets or sets the customers balance
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// Gets or sets the customer address
        /// </summary>
        [Required]
        public CustomerAddress Address { get; set; }
    }
}
